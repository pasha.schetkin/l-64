<?php

namespace Database\Seeders;

use App\Models\Cafe;
use App\Models\Dish;
use Illuminate\Database\Seeder;

class CafeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cafe::factory()->count(5)
            ->has(Dish::factory()->count(8))
            ->create();
    }
}
