<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:2', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->route('user')),
                'max:255'],
            'password' => ['required', 'string', 'min:8',],
            'is_admin' => ['nullable', 'numeric', 'min:0', 'max:1'],
            'phone' => ['nullable', 'string', 'min:4', 'max:64'],
            'address' => ['nullable', 'string', 'min:4', 'max:128']
        ];
    }
}
