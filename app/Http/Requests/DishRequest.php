<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:2', 'max:125', 'string'],
            'description' => ['required', 'min:10', 'max:1024', 'string'],
            'price' => ['required', 'max:20', 'numeric'],
            'image' => ['required', 'image'],
            'cafe_id' => ['required']
        ];
    }
}
