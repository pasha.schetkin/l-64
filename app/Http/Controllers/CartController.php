<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function add(Dish $dish, Request $request): RedirectResponse
    {
        $sessionData = session()->get('cart.'. $dish->cafe->id, []);
        $existent = null;
        foreach ($sessionData as &$item){
            if ($item['dish_id'] == $dish->id){
                $existent = true;
                $item['quantity'] += $request->quantity ?: 1;
            }
        }
        if (!$existent){
            $sessionData[] = [
                'dish_id' => $dish->id,
                'quantity' => $request->quantity ?: 1
            ];
        }
        session()->put('cart.' . $dish->cafe->id, $sessionData);
        return redirect()->back();
    }

    public function remove(Dish $dish): RedirectResponse
    {
        $cart = session()->get('cart.' . $dish->cafe->id, []);
        $cart = array_values(array_filter($cart, fn ($item) => $item['dish_id'] != $dish->id));
        session()->put('cart.' . $dish->cafe->id, $cart);
        return redirect()->back();
    }
}
