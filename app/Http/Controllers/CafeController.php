<?php

namespace App\Http\Controllers;

use App\Models\Cafe;
use App\Models\Dish;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CafeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $cafes = Cafe::paginate(6);
        return view('cafes.index', compact('cafes'));
    }

    /**
     * Display the specified resource.
     *
     * @param Cafe $cafe
     * @return Application|Factory|View
     */
    public function show(Cafe $cafe): View|Factory|Application
    {
        $sessionData = session()->get('cart.'. $cafe->id, []);
        $totalPrice = 0;
        foreach ($sessionData as &$item) {
            $item['dish'] = Dish::find($item['dish_id']);
            $totalPrice += $item['dish']->price * $item['quantity'];
        }
        $cart = [
            'dishes' => $sessionData,
            'total_price' => $totalPrice
        ];
        return view('cafes.show', compact('cafe', 'cart'));
    }
}
