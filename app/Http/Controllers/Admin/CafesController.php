<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CafeRequest;
use App\Models\Cafe;
use App\Models\Dish;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class CafesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $cafes = Cafe::all();
        return view('admin.cafes.index', compact('cafes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('admin.cafes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CafeRequest $request
     * @return RedirectResponse
     */
    public function store(CafeRequest $request): RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['image'] = $request->file('image')->store('pictures/cafes', 'public');
        }
        Cafe::create(array_merge($data, ['user_id' => Auth::id()]));
        return redirect()->route('admin.cafes.index')->with('message', 'Новое кафе добалено!');
    }

    /**
     * Display the specified resource.
     *
     * @param Cafe $cafe
     * @return Application|Factory|View
     */
    public function show(Cafe $cafe): View|Factory|Application
    {
        $dishes = Dish::where('cafe_id', $cafe->id)->get();
        return view('admin.cafes.show', compact('cafe', 'dishes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Cafe $cafe
     * @return Application|Factory|View
     */
    public function edit(Cafe $cafe): View|Factory|Application
    {

        return view('admin.cafes.edit', compact('cafe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CafeRequest $request
     * @param Cafe $cafe
     * @return RedirectResponse
     */
    public function update(CafeRequest $request, Cafe $cafe): RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = $file->store('pictures/cafes', 'public');
            $data['image'] = $path;
        }
        $cafe->update($data);
        return redirect()->route('admin.cafes.index', compact('cafe'))->with('message', 'Кафе обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cafe $cafe
     * @return RedirectResponse
     */
    public function destroy(Cafe $cafe): RedirectResponse
    {
        $cafe->delete();
        return redirect()->route('admin.cafes.index')->with('message', 'Кафе успешно удалено!');
    }
}
