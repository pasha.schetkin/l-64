<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DishRequest;
use App\Models\Cafe;
use App\Models\Dish;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class DishesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $dishes = Dish::all();
        return view('admin.dishes.index', compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        $cafes = Cafe::all();
        return view('admin.dishes.create', compact('cafes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DishRequest $request
     * @return RedirectResponse
     */
    public function store(DishRequest $request): RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['image'] = $request->file('image')->store('pictures/dishes', 'public');
        }
        Dish::create(array_merge($data, ['user_id' => Auth::id()]));
        return redirect()->route('admin.dishes.index')->with('message', 'Новое блюдо добалено!');
    }

    /**
     * Display the specified resource.
     *
     * @param Dish $dish
     * @return Application|Factory|View
     */
    public function show(Dish $dish): View|Factory|Application
    {
        return view('admin.dishes.show', compact('dish'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Dish $dish
     * @return Application|Factory|View
     */
    public function edit(Dish $dish): View|Factory|Application
    {
        $cafes = Cafe::all();
        return view('admin.dishes.edit', compact('dish', 'cafes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DishRequest $request
     * @param Dish $dish
     * @return RedirectResponse
     */
    public function update(DishRequest $request, Dish $dish): RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = $file->store('pictures/cafes', 'public');
            $data['image'] = $path;
        }
        $dish->update($data);
        return redirect()->route('admin.dishes.index', compact('dish'))->with('message', 'Блюдо обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Dish $dish
     * @return RedirectResponse
     */
    public function destroy(Dish $dish): RedirectResponse
    {
        $dish->delete();
        return redirect()->route('admin.dishes.index')->with('message', 'Блюдо успешно удалено!');
    }
}
