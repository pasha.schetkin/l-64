<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $dishes = Dish::paginate(12);
        return view('dishes.index', compact('dishes'));
    }

    /**
     * Display the specified resource.
     *
     * @param Dish $dish
     * @return Application|Factory|View
     */
    public function show(Dish $dish): View|Factory|Application
    {
        return view('dishes.show', compact('dish'));
    }
}
