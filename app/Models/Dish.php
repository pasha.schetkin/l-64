<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static paginate(int $int)
 */
class Dish extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'price', 'image', 'cafe_id'];

    public function cafe(): BelongsTo
    {
        return $this->belongsTo(Cafe::class);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
