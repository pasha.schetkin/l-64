<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static paginate(int $int)
 */
class Cafe extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'image'];

    public function dishes(): HasMany
    {
        return $this->hasMany(Dish::class);
    }
}
