<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\CartController;
use App\Http\Controllers\DishController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CafeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CafeController::class, 'index']);

Route::resource('cafes', CafeController::class)
    ->only(['index', 'show']);
Route::resource('dishes', DishController::class)
    ->only(['index', 'show']);
Route::post('cart/{dish}', [CartController::class, 'add'])
    ->name('cart.add');
Route::delete('cart/{dish}', [CartController::class, 'remove'])
    ->name('cart.remove');

Auth::routes();

Route::prefix('admin')->name('admin.')
    ->middleware(['auth', 'is_admin'])->group( function () {
    Route::resources([
        'dishes' => Admin\DishesController::class,
        'cafes' => Admin\CafesController::class,
        'users' => Admin\UserController::class
    ]);
});
