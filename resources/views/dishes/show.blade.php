@extends('layouts.app')

@section('content')
    <div class="col-xxl-8 px-4 py-5">
        <h2>Просмотр блюда</h2>
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
            <div class="col-10 col-sm-8 col-lg-6">
                <img src="{{asset('storage/' . $dish->image)}}" class="d-block mx-lg-auto img-fluid shadow-lg rounded-3" alt="Книга" width="700" height="500" loading="lazy">
            </div>
            <div class="col-lg-6">
                <h3 class="display-6 fw-bold lh-1 mb-4">{{$dish->name}}</h3>
                <p class="lead text-muted">
                    {{$dish->description}}
                </p>
            </div>
        </div>
    </div>
@endsection
