@extends('layouts.app')

@section('content')
    <h3 class="display-6 fw-bold lh-1 text-center mt-5 text-danger">Блюда</h3>
    <div class="row row-cols-1 row-cols-md-3 g-4 mb-5 mt-3 pb-5">
        @foreach($dishes as $dish)
            <div class="col-sm-6 col-lg-4 col-xl-3">
                <div class="card shadow h-100 hover">
                    <a href="{{route('dishes.show', ['dish'=> $dish])}}">
                        <div class="position-relative" >
                            <img src="{{asset('/storage/' . $dish->image)}}" alt="{{$dish->image}}"
                                 class="card-img-top " style="height: 250px">

                            <div class="card-img-overlay p-3 z-index-1">
                                <div class="badge text-bg-dark"><i
                                        class="fa-solid fa-building-columns fa-fw text-warning"></i> Museum
                                </div>
                                <div class="badge text-bg-success">Open</div>
                            </div>
                        </div>
                    </a>

                    <div class="card-body">
                        <h5 class="card-title me-2 text-capitalize">Ресторан: {{$dish->cafe->name}}</h5>
                        <hr>
                        <form class="mb-3 create-basket" method="POST" action="{{route('cart.add', ['dish' => $dish])}}">
                            @csrf
                            <input type="hidden" value="{{$dish->id}}" name="dish_id">
                            <div class="card--item-action">
                                <div class="cost">{{$dish->cost}}<i>&nbsp;</i>
                                </div>
                                <div class="form-outline">
                                    <label class="form-label" for="typeNumber">Количество:</label>
                                    <input name="quantity" value="1" type="number" id="typeNumber" class="form-control my-3" />
                                </div>
{{--                                <button type="submit" class="create-basket-btn status active bay_item " >--}}
{{--                                    Заказать--}}
{{--                                </button>--}}
                                <button type="submit" class="btn btn-success">Заказать</button>
                            </div>
                        </form>
                    </div>

                    <div class="card-footer border-top">
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="{{route('dishes.show', ['dish'=> $dish])}}" class="btn btn-link p-0 mb-0 text-decoration-none">
                                Просмотр блюда
                                <i class="bi bi-arrow-right ms-1"></i></a>
                            <a href="#" class="h6 mb-0 z-index-2"><i class="fa-regular fa-heart"></i></a>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="justify-content-md-center px-5 mb-5">
        <div class="col-md-auto text-danger">
            {{ $dishes->links() }}
        </div>
    </div>

    <footer class="d-flex flex-wrap justify-content-between
    align-items-center py-3 my-4 border-top border-danger">
        <div class="col-md-4 d-flex align-items-center">
            <span class="mb-3 mb-md-0 ">© 2022 Company, Inc</span>
        </div>
    </footer>
@endsection
