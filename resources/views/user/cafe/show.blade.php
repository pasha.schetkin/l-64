@extends('layouts.app')
@section('content')
    <div class="" style="display: block; ">
        <div id="carouselExampleAutoplaying" class="carousel slide p-0" data-bs-ride="carousel">
            <div class="carousel-inner py-0 " style="max-height: 460px">
                <div class="carousel-item active">
                    <img class="d-block w-100 h-25" src="{{asset('/storage/' . $cafe->image)}}"
                         alt="{{$cafe->image}}">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-25" src="{{asset('/storage/' . $cafe->image)}}"
                         alt="{{$cafe->image}}">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 h-25" src="{{asset('/storage/' . $cafe->image)}}"
                         alt="{{$cafe->image}}">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying"
                    data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying"
                    data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

        <div class="" style="">
            <div class="container col-xxl-8 px-4 py-5">
                <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
                    <div class="col-10 col-sm-8 col-lg-6">
                        <img src="{{asset('/storage/' . $cafe->image)}}" alt="{{$cafe->image}}"
                             class="d-block mx-lg-auto img-fluid" width="700" height="500" loading="lazy">
                    </div>
                    <div class="col-lg-6">
                        <h2 class="display-5 fw-bold lh-1 mb-3">{{$cafe->name}}</h2>
                        <p class="lead">{{$cafe->desc}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
