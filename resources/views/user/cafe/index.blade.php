@extends('layouts.app')
@section('content')
    <div>
        <div class="container d-flex row m-0 p-0 text-center ">
            @foreach($cafes as $cafe)
                <div class="mb-2 col-6 mt-5" style="height: 400px">
                    <div class="bg-secondary">
                        <a href="{{route('user.cafes.show', ['cafes' => $cafe])}}"
                           class="col-auto d-none d-lg-block text-decoration-none text-black">
                            <div
                                class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative ">
                                <div class="col p-4 d-flex flex-column position-static" style="max-height: 300px">
                                    <strong class="d-inline-block mb-2 text-primary"></strong>
                                    <h3 class="mb-0">{{$cafe->name}}</h3>
                                    <div class="mb-1 text-muted">Nov 12</div>
                                    <p class="card-text mb-auto " style="max-height: 300px">{{$cafe->desc}}</p>
                                </div>

                                <div class="col-auto d-none d-lg-block">
                                    <img fill="#eceeef" dy=".3em"
                                         src="{{asset('/storage/' . $cafe->image)}}" alt="{{$cafe->image}}"
                                         style="height: 300px; width: 350px">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
