@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container mb-4">
        <h2>Блюда для Гурманов</h2>
    </div>

    <form class="mb-5" method="post" action="{{ route('admin.dishes.update', ['dish' => $dish]) }}" enctype="multipart/form-data">
        @method('put')
        @csrf
        <h1>Изменить блюдо</h1>
        <div class="form-group my-3">
            <label for="name">Название блюда</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$dish->name}}">
        </div>

        <div class="form-group my-3">
            <label for="description">Описание блюда</label>
            <textarea rows="7" class="form-control" id="description" name="description" >{{$dish->description}}</textarea>
        </div>

        <div class="form-group my-4">
            <label for="cafe_id">Рестораны</label>
            <select name="cafe_id" class="custom-select">
                {{old('cafe_id')}}
                @foreach($cafes as $cafe)
                    <option value="{{$cafe->id}}">{{$cafe->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group my-3">
            <label for="price">Цена</label>
            <input class="form-control" id="price" name="price" value="{{$dish->price}}">
        </div>

        <img src="{{asset('/storage/' . $dish->image)}}" alt="{{$dish->image}}" style="width:300px;height:230px;">
        <br>
        <div class="form-group my-4">
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Choose file</label>
                <input type="file" class="custom-file-input form-control" id="customFile" name="image" value="{{old('image')}}">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Принять изменения</button>
    </form>
@endsection
