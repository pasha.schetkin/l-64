@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container mb-4">
        <h2>Блюда для гурманов</h2>
    </div>

    <form method="post" action="{{ route('admin.dishes.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Название блюда</label>
            <input type="text" class="form-control" id="name" name="name" value="">
            @error('name')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="description">Описание Блюда</label>
            <textarea rows="7" class="form-control" id="description" name="description"></textarea>
            @error('description')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group my-4">
            <label for="cafe_id">Рестораны</label>
            <select name="cafe_id" class="custom-select">
                @foreach($cafes as $cafe)
                <option value="{{$cafe->id}}">{{$cafe->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="price">Цена</label>
            <input class="form-control" id="price" name="price" value="">
            @error('cost')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group my-4">
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Выбрать файл</label>
                <input type="file" class="custom-file-input form-control" id="customFile" name="image">
            </div>
            @error('image')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Принять</button>
    </form>
@endsection
