@extends('layouts.app')
@section('content')
    <div class="container mb-4">
        <h2>Описание блюда </h2>

    <div class="card " style="width: 18rem;">
        <div class="card-header">Детали блюда: {{$dish->name}}</div>
        <img class="card-img-top" src="{{asset('/storage/' . $dish->image)}}" alt="{{$dish->image}}">
        <div class="card-body">
            <h5 class="card-title">{{$dish->name}}</h5>
            <p class="card-text">
                    {{$dish->description}}
            </p>
        </div>
        <footer class="blockquote-footer mb-3">
            Цена: {{number_format($dish->price, 2)}}
        </footer>
    </div>

</div>
@endsection
