@extends('layouts.app')

@section('content')
    <h3 class="mt-5">Users</h3>
    @if (session('message'))
        <div class="alert alert-success mb-3" role="alert">
            {{ session('message') }}
        </div>
    @endif

    <a href="{{route('admin.users.create')}}" class="btn btn-primary me-3">Создать нового админа или юзера </a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Имя</th>
            <th scope="col">Email</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{route('admin.users.edit', compact('user'))}}" class="btn btn-primary me-3">Изменить</a>
                        <form action="{{route('admin.users.destroy', compact('user'))}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
