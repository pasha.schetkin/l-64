<div class="container">

    <div style="padding-bottom: 30px;">
        <h3>Блюда для гурманов</h3>
        <a href="{{route("admin.dishes.create")}}" type="button" class="btn btn-outline-primary">
            Добавить новое блюдо
        </a>
    </div>

    <table class="table" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Фото</th>
            <th scope="col">Цена</th>
            <th scope="col">Действие</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dishes as $dish)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>
                    {{$dish->name}}
                </td>

                <td class="" style="max-width: 200px">
                    <img class="card-img-top" height="200" src="{{asset('/storage/' . $dish->image)}}"
                         alt="{{$dish->image}}">
                </td>

                <td class="px-5">
                    {{$dish->price}}
                </td>
                <td>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <a class="btn btn-outline-warning"
                                   href="{{route('admin.dishes.edit', ['dish' => $dish])}}">
                                    Изменить
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <form method="post" action="{{route('admin.dishes.destroy', ['dish' => $dish])}}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger">Удалить</button>
                                </form>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="{{route('admin.dishes.show', ['dish' => $dish])}}">
                                    <button type="submit" class="btn btn-outline-danger">
                                        Показать
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>


