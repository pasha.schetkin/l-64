@extends('layouts.app')


@section('content')
    <h3 class="mt-5">Создать новое кафе</h3>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('admin.cafes.update', compact('cafe'))}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="mb-3">
            <label>
                Название
                <input type="text" class="form-control" name="name" value="{{$cafe->name}}">
            </label>
        </div>

        <div class="mb-3">
            <label>
                Описание
                <textarea name="description" cols="20" rows="3"
                          class="form-control">{{$cafe->description}}</textarea>
            </label>
        </div>

        <img class="mb-3" src="{{asset('/storage/' . $cafe->image)}}" alt="{{$cafe->image}}" style="width:300px;height:230px;">
        <br>
        <div class="mb-3">
            <label>
                <input type="file" required name="image" class="form-control" value="{{old($cafe->image)}}">
            </label>
        </div>

        <button class="btn btn-success">Принять</button>
    </form>
@endsection
