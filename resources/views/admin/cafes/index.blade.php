@extends('layouts.app')

@section('content')
    <h3 class="mt-5">КаФе</h3>
    @if (session('message'))
        <div class="alert alert-success mb-3" role="alert">
            {{ session('message') }}
        </div>
    @endif
    <a href="{{route('admin.cafes.create')}}">Создать новое кафе</a>
    <table class="table">
        <thead>
            <tr>

                <th scope="col">Фото</th>
                <th scope="col">Название</th>
                <th scope="col">Описание</th>
                <th scope="col">Действие</th>
            </tr>
        </thead>

        <tbody>
        @foreach($cafes as $cafe)
            <tr>
                <td><img class="card-img-top" src="{{asset('storage/' . $cafe->image)}}" alt="{{$cafe->image}}" style="width: 250px"></td>
                <td>{{$cafe->name}}</td>
                <td>{{$cafe->description}}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{route('admin.cafes.show', compact('cafe'))}}" class="btn btn-secondary me-3">Показать</a>
                        <a href="{{route('admin.cafes.edit', compact('cafe'))}}" class="btn btn-primary me-3">Изменить</a>
                        <form action="{{route('admin.cafes.destroy', compact('cafe'))}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
