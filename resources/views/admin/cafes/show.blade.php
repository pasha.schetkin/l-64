@extends('layouts.app')

@section('content')
    <div class="container mb-4">
        <h2>Тут можно вкусно поесть</h2>
    </div>
    <div class="card " style="width: 24rem;">
        <div class="card-header">
            Деталь жанра: {{$cafe->name}}
        </div>
        <img class="card-img-top" src="{{asset('storage/' . $cafe->image)}}" alt="{{$cafe->image}}">

        <div class="card-body">
            <h5 class="card-title">{{$cafe->description}}</h5>
            <a href="{{route('admin.cafes.index')}}" class="btn btn-default btn-sm">Назад</a>|
            <a href="{{route('admin.cafes.edit', ['cafe' => $cafe])}}" class="btn btn-primary btn-sm">Изменить</a>
        </div>
    </div>

    <p class="mt-5 mb-5">{{$cafe->desc}}</p>

    <h1 class="mb-4">Блюдa в ассортименте</h1>

    @include('admin.parties.index')

@endsection
