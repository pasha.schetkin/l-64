@extends('layouts.app')
@section('content')
    <h3 class="mt-5">Создать новое кафе</h3>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('admin.cafes.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label>
                Название
                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                @error('name')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </label>
        </div>

        <div class="mb-3">
            <label>
                Описание
                <textarea name="description" cols="20" rows="3"
                          class="form-control">{{old('description')}}</textarea>
                @error('description')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </label>
        </div>

        <div class="mb-3">
            <label>
                <input type="file" required name="image" class="form-control" value="{{old('image')}}">
                @error('image')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </label>
        </div>

        <button class="btn btn-success">Принять</button>
    </form>
@endsection
