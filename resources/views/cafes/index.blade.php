@extends('layouts.app')
@section('content')
    <h3 class="display-6 fw-bold lh-1 text-center mt-5 text-danger">Заведения</h3>
    {{--    <div class="row row-cols-1 row-cols-md-3 g-4 mb-5 mt-3 pb-5">--}}
    <div class="container d-flex row m-0 p-0 text-center ">


        @foreach($cafes as $cafe)
            <div class="mb-2 col-6 mt-3" style="height: 400px">
                <div class="">
                    <a href="{{route('cafes.show', ['cafe' => $cafe])}}"
                       class="col-auto d-none d-lg-block text-decoration-none text-black">
                        <div
                            class="row g-0 border rounded overflow-hidden flex-md-row mb-3 shadow-sm h-md-250 position-relative ">
                            <div class="col p-4 d-flex flex-column position-static" style="max-height: 300px">
                                <strong class="d-inline-block mb-2 text-primary"></strong>
                                <h3 class="mb-0">{{$cafe->name}}</h3>
                                <div class="mb-1 text-muted fs-3"><string style="letter-spacing: -5px">------</string>  <i class="bi bi-pinterest"></i><string style="letter-spacing: -5px">------</string></div>
                                <p class="card-text mb-auto " style="max-height: 300px">{{$cafe->description}}</p>
                            </div>

                            <div class="col-auto d-none d-lg-block">
                                <img fill="#eceeef" dy=".3em"
                                     src="{{asset('/storage/' . $cafe->image)}}" alt="{{$cafe->image}}"
                                     style="height: 300px; width: 350px">
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>

    <div class="justify-content-md-center px-5 mb-5">
        <div class="col-md-auto text-danger">
            {{ $cafes->links() }}
        </div>
    </div>

    <footer class="d-flex flex-wrap justify-content-between
    align-items-center py-3 my-4 border-top border-danger">
        <div class="col-md-4 d-flex align-items-center">
            <span class="mb-3 mb-md-0 ">© 2022 Company, Inc</span>
        </div>
    </footer>
@endsection
