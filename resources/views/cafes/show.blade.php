@extends('layouts.app')

@section('content')
    <div class="row flex-row">
        <div class="col-xxl-8 px-4 py-5">
            <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
                <div class="col-10 col-sm-8 col-lg-6">
                    <img src="{{asset('storage/' . $cafe->image)}}"
                         class="d-block mx-lg-auto img-fluid shadow-lg rounded-3" alt="Книга" width="700" height="500"
                         loading="lazy">
                </div>
                <div class="col-lg-6">
                    <h3 class="display-6 fw-bold lh-1 mb-4">{{$cafe->name}}</h3>
                    <p class="lead text-muted">
                        {{$cafe->description}}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xxl-4 border border-2 border-info px-4 py-2 rounded-5">
            <h3><i class="bi bi-cart2 text-info fs-2"></i></h3>
            <table class="table">
                <thead>
                <tr class="border-info">
                    <th scope="col">Позиция</th>
                    <th scope="col">Количество</th>
                    <th scope="col">Цена</th>
                </tr>
                </thead>

                <tbody>
                @foreach($cart['dishes'] as $dish)
                    <tr class="border-info">
                        <td>{{$dish['dish']->name}}</td>
                        <td>{{$dish['dish']->quantity}}</td>
                        <td>{{$dish['dish']->price}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>
    <h3 class="display-6 fw-bold lh-1 text-center mt-5 text-danger text-capitalize">Блюда в меню</h3>

    <div class="row row-cols-1 row-cols-md-3 g-4 mb-5 mt-3 pb-5">
        @foreach($cafe->dishes as $dish)
            <div class="col-sm-6 col-lg-4 col-xl-3">
                <div class="card shadow h-100 hover">
                    <a href="{{route('dishes.show', ['dish'=> $dish])}}">
                        <div class="position-relative">
                            <img src="{{asset('/storage/' . $dish->image)}}" alt="{{$dish->image}}"
                                 class="card-img-top " style="height: 250px">

                            <div class="card-img-overlay p-3 z-index-1">
                                <div class="badge text-bg-dark"><i
                                        class="fa-solid fa-building-columns fa-fw text-warning"></i> Museum
                                </div>
                                <div class="badge text-bg-success">Open</div>
                            </div>
                        </div>

                    </a>

                    <div class="card-body">
                        <h5 class="card-title me-2 text-capitalize">Ресторан: {{$dish->cafe->name}}</h5>
                        <hr>
                        <form class="mb-3 create-basket" method="POST"
                              action="{{route('cart.add', ['dish' => $dish])}}">
                            @csrf
                            <input type="hidden" value="{{$dish->id}}" name="dish_id">
                            <div class="card--item-action">
                                <div class="cost">{{$dish->price}}<i>&nbsp;</i>
                                </div>
                                <div class="form-outline">
                                    <label class="form-label" for="typeNumber">Количество:</label>
                                    <input name="quantity" value="1" type="number" id="typeNumber"
                                           class="form-control my-3"/>
                                </div>
                                <button type="submit" class="btn btn-success">Заказать</button>
                            </div>
                        </form>
                    </div>

                    <div class="card-footer border-top">
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="{{route('dishes.show', ['dish'=> $dish])}}"
                               class="btn btn-link p-0 mb-0 text-decoration-none">
                                Просмотр блюда
                                <i class="bi bi-arrow-right ms-1"></i></a>
                            <a href="#" class="h6 mb-0 z-index-2"><i class="fa-regular fa-heart"></i></a>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
