<?php

namespace Tests\Feature;

use App\Models\Cafe;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SessionTest extends TestCase
{
   use RefreshDatabase;

    /**
     * @group session
     */
    public function test_add_card_user()
   {
       $user = User::factory()->create();
       $dish = Cafe::factory()->count(1)->has(Dish::factory()->count(1))->create();
       $response = $this->actingAs($user)->withSession(['cart' => $dish[0]])->post("/cart/{$dish[0]->id}");
   }
}
