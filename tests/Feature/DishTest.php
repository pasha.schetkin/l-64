<?php

namespace Tests\Feature;

use App\Models\Cafe;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DishTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @group dishes
     */
    public function test_admin_index()
    {
        $response = $this->get('/admin/dishes/');
        $response->assertStatus(302);
    }

    /**
     * @group dishes
     */
    public function test_admin_index_register()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/admin/dishes/');
        $response->assertStatus(403);
    }

    /**
     * @group dishes
     */
    public function test_admin_destroy()
    {
        $user = User::factory()->create();
        $dish = Cafe::factory()->count(1)->has(Dish::factory()->count(1))->create();
        $response = $this->actingAs($user)->delete('/admin/dishes/' . $dish[0]->id);
        $response->assertStatus(403);
    }

    /**
     * @group dishes
     */
    public function test_admin_update()
    {
        $user = User::factory()->create();
        $dish = Cafe::factory()->count(1)->has(Dish::factory()->count(1))->create();
        $response = $this->actingAs($user)->put('/admin/dishes/' . $dish[0]->id);
        $response->assertStatus(403);
    }

    /**
     * @group dishes
     */
    public function test_admin_store()
    {
        $user = User::factory()->create();
        $dish = Cafe::factory()->count(1)->has(Dish::factory()->count(1))->create();
        $response = $this->actingAs($user)->post('/admin/dishes/', $dish[0]->toArray());
        $response->assertStatus(403);
    }
}
