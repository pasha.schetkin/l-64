<?php

namespace Tests\Feature;

use App\Models\Cafe;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CafeControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @group cafes
     */
    public function test_admin_index()
    {
        $response = $this->get('/admin/cafes/');
        $response->assertStatus(302);
    }

    /**
     * @group cafes
     */
    public function test_admin_index_register()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/admin/cafes/');
        $response->assertStatus(403);
    }

    /**
     * @group cafes
     */
    public function test_admin_destroy()
    {
        $user = User::factory()->create();
        $cafe = Cafe::factory()->create();
        $response = $this->actingAs($user)->delete('/admin/cafes/' . $cafe->id);
        $response->assertStatus(403);
    }

    /**
     * @group cafes
     */
    public function test_admin_update()
    {
        $user = User::factory()->create();
        $cafe = Cafe::factory()->create();
        $response = $this->actingAs($user)->put('/admin/cafes/' . $cafe->id);
        $response->assertStatus(403);
    }

    /**
     * @group cafes
     */
    public function test_admin_store()
    {
        $user = User::factory()->create();
        $cafe = Cafe::factory()->create();
        $response = $this->actingAs($user)->post('/admin/cafes/', $cafe->toArray());
        $response->assertStatus(403);
    }
}
