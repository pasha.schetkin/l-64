<?php

namespace Tests\Feature;

use App\Models\Cafe;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @group admin
     */
    public function test_admin_index()
    {
        $response = $this->get('/admin/users/');
        $response->assertStatus(302);
    }


    /**
     * @group admin
     */
    public function test_admin_index_register()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/admin/users/');
        $response->assertStatus(403);
    }


    /**
     * @group admin
     */
    public function test_admin_destroy()
    {
        $user = User::factory()->create();
        $another = User::factory()->create();
        $response = $this->actingAs($user)->delete('/admin/users/' . $another->id);
        $response->assertStatus(403);
    }


    /**
     * @group admin
     */
    public function test_admin_update()
    {
        $user = User::factory()->create();
        $another = User::factory()->create();
        $response = $this->actingAs($user)->put('/admin/users/' . $another->id);
        $response->assertStatus(403);
    }


    /**
     * @group admin
     */
    public function test_admin_store()
    {
        $user = User::factory()->create();
        $another = Cafe::factory()->create();
        $response = $this->actingAs($user)->post('/admin/users/', $another->toArray());
        $response->assertStatus(403);
    }
}
